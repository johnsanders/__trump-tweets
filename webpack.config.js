var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require("path");
var CreateDistHtml = new HtmlWebpackPlugin({
	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'body'
});
module.exports = {
	entry: [ 
		'./app/index.js'
	],
	output: { 
		path: __dirname + '/dist',
		filename: "bundle.js"
	},
	devtool: "source-map",
	stats: {
		colors:true,
		reasons:true,
		chunks:true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				include:__dirname + '/app',
				loader: "babel-loader",
			},
			{ 
				test: /\.css$/, 
				use: [
					"style-loader",
					"css-loader"
				]
			},
			{
				test: /\.(png|jpg|jpeg|woff|woff2|ttf|eot|svg|vert|frag)(\?v=\d+\.\d+\.\d+)?$/, 
				loader: 'file-loader'
			},
		]
	},
	plugins: [
		CreateDistHtml,
	]
	
}
