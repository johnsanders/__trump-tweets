class Edge {
	constructor (source, target) {
		this.source = source;
		this.target = target;
		this.data = {};
	}
}
export default Edge;
