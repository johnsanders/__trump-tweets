import Node from "./Node";
class TweetNode extends Node {
	constructor(id, info, layout) {
		super(id);
		this.data.info = info;
		this.data.type = "tweet";
		this.active = false;
		this.categories = [];
		this.myLayout = layout;
	}
	addCategory(category) {
		this.categories.push(category);
	}
	setActive(state) {
		let newState;	
		if ( Array.isArray( Array ) ){
			newState = this.isActive(state);
		} else if ( typeof state === "boolean" ){
			newState = state;
		}
		if (this.active != newState){
			this.active = newState;
			this.myLayout.init();
		}
	}
	isActive(categoriesState) {
		let nodeIsActive = false;
		this.categories.forEach(function(category) {
			if (categoriesState[category.id])	{
				nodeIsActive = true;
			}
		});
		return nodeIsActive;
	}
}
export default TweetNode;
