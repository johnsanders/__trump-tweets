import Graph from "./Graph";
import CategoryNode from "./CategoryNode";
import TrumpStructure from "../../config/TrumpStructure";
import ForceDirected from "../../vendors/force-directed-layout";
import DataNodeAndEdgeFactory from "../../factories/DataNodeAndEdgeFactory"; 
class TrumpGraph extends Graph {
	constructor(cb) {
		super();
		this.addCategoryNodes = this.addCategoryNodes.bind(this);
		this.tick = this.tick.bind(this);

		const trumpStructure = new TrumpStructure();
		this.categories = trumpStructure.data;
		this.getTweets().then((tweets) => this.init(tweets, cb));
	}
	init(tweets, cb) {
		this.layout = new ForceDirected(this, {width:1500, height:1500, iterations:100000, layout:"3d"});
		this.addCategoryNodes(this.categories);
		const nodesAndEdges = DataNodeAndEdgeFactory.createTweets(tweets, this.categories, this.nodes, this.layout);
		nodesAndEdges.nodes.forEach( (node) => {
			this.addNode(node);
		});
		nodesAndEdges.edges.forEach( (edge) => {
			this.addEdge(edge[0], edge[1]);
		} );

		this.addTweetData(this.nodes);
		cb();
	}
	getTweets() {
		const url = "/apps/flashSQLinterface/read3.php?table=twitter_archive&where=username=%22realDonaldTrump%22";
		return fetch(url).then(response => response.json());
	}
	addTweetData(nodes) {
		const getMinMax = (items, minMax, property) => {
			Math[minMax].apply(Math, items.map(function(item) {
				return item.data.type=="tweet" ? item.data.info.data[property] : null;
			}));
		};

		nodes.forEach(function(node) {
			if(node.data.type == "tweet" && typeof node.data.info.data == "string") {
				node.data.info.data = JSON.parse(node.data.info.data);
			}
		});
		const minFav = getMinMax(nodes, "max", "favorite_count");
		const maxFav = getMinMax(nodes, "min", "favorite_count");
		const minRt = getMinMax(nodes, "min", "retweet_count");
		const maxRt = getMinMax(nodes, "max", "retweet_count");

		if (minFav && maxFav && minRt && maxRt) {
			nodes.forEach(function(node) {
				if(node.data.type == "tweet") {
					node.data.info.favScale = (node.data.info.data.favorite_count - minFav) / maxFav;
					node.data.info.retweetScale = (node.data.info.data.retweet_count - minRt) / maxRt;
				}
			});
		}
	}
	addCategoryNodes(categories, parentNode) {
		categories.forEach( (category) => {
			const node = new CategoryNode(this.nodes.length, category, this.layout);
			this.addNode(node);
			if (parentNode) {
				this.addEdge(parentNode, node);
			}
			if (category.children.length > 0) {
				this.addCategoryNodes(category.children, node);
			}
		});
	}
		
	tick() {
		this.layout.generate();
	}
}
export default TrumpGraph;
