import Edge from "./Edge";
class Graph {
	constructor(options) {
		this.options = options || {};
		this.nodeSet = {};
		this.nodes = [];
		this.edges = [];
		this.layout = undefined;
	}
	addNode (node) {
		if(this.nodeSet[node.id] === undefined && !this.reached_limit()) {
			this.nodeSet[node.id] = node;
			this.nodes.push(node);
			return true;
		}
		return false;
	}
	getNode (node_id) {
		return this.nodeSet[node_id];
	}
	addEdge(source, target) {
		if(source.addConnectedTo(target) === true) {
			var edge = new Edge(source, target);
			this.edges.push(edge);
			return true;
		}
		return false;
	}
	reached_limit() {
		if(this.options.limit !== undefined) {
			return this.options.limit <= this.nodes.length;
		} else {
			return false;
		}
	}
}
export default Graph;
