class Node {
	constructor(node_id) {
		this.id = node_id;
		this.nodesTo = [];
		this.nodesFrom = [];
		this.position = {};
		this.data = {};
	}
	addConnectedTo (node) {
		if(this.connectedTo(node) === false) {
			this.nodesTo.push(node);
			return true;
		}
		return false;
	}
	connectedTo (node) {
		for(var i=0; i < this.nodesTo.length; i++) {
			var connectedNode = this.nodesTo[i];
			if(connectedNode.id == node.id) {
				return true;
			}
		}
		return false;
	}
}
export default Node;
