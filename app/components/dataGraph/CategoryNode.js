import Node from "./Node";
class CategoryNode extends Node {
	constructor(id, info, layout) {
		super(id);
		this.data.type = "category";
		this.data.info = info;
		this.active = false;
		this.myLayout = layout;
	}
	setActive(state){
		let newState;
		if (state.constructor === Array){
			newState = this.isActive(state);
		} else if (typeof state === "boolean"){
			newState = state;
		}
		if (this.active != newState){
			this.active = newState;
			this.myLayout.init();
		}
	}
	isActive(categoriesState) {
		return categoriesState[this.data.info.id];
	}
}
export default CategoryNode;
