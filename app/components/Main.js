import React from "react";
import TrumpGraph from "./dataGraph/TrumpGraph.js";
import ThreeWorld from "./three/ThreeWorld";
import ReactContainer from "./ui/ReactContainer";
class Main extends React.Component {
	constructor (props) {
		super(props);
		this.windowObject = props.windowObject;
		this.state = {
			activeTweet:null,
			activeTweetNodePos:{x:30, y:30},
			threeReady:false
		};
		this.onDataGraphReady = this.onDataGraphReady.bind(this);
		this.animate = this.animate.bind(this);
		this.onNodeClick = this.onNodeClick.bind(this);
		this.onNodeHover = this.onNodeHover.bind(this);
		this.onCategoryButtonClick = this.onCategoryButtonClick.bind(this);

	}
	componentDidMount() {
		this.dataGraph = new TrumpGraph(this.onDataGraphReady);
	}
	onDataGraphReady() {
		this.domElement3D = this.windowObject.document.getElementById("ThreeScene");
		this.threeWorld = new ThreeWorld(this.dataGraph, this.domElement3D,
			this.windowObject.innerWidth-200, this.windowObject.innerHeight-50,
			this.onNodeClick, this.onNodeHover);
		this.domElement3D.appendChild(this.threeWorld.domElement);
		this.threeWorld.onDomReady();

		this.setState({threeReady:true});
		this.animate();
	}
	onNodeHover(nodeData, node3D) {
		if(nodeData) {
			this.setState({
				activeTweet: nodeData.data.info.data,
				activeTweetNodePos: this.getScreenCoordinatesFrom3D(
					node3D.position.clone(), this.domElement3D.offsetWidth, this.domElement3D.offsetHeight)
			});
		} else {
			this.setState({
				activeTweet: null,
				activeTweetNodePos: {x:0, y:0}
			});
		}
	}
	onNodeClick(nodeData, node3D) {
		this.setState({
			activeTweetText: nodeData.data.info.data,
			activeTweetNodePos: this.getScreenCoordinatesFrom3D(node3D.position.clone())
		});
	}
	getScreenCoordinatesFrom3D(pos, containerWidth, containerHeight) {
		this.projectVector = pos.project(this.threeWorld.threeCore.camera);
		this.projectVector.x = (this.projectVector.x + 1) / 2 * containerWidth;
		this.projectVector.y = -(this.projectVector.y - 1) / 2 * containerHeight;
		return this.projectVector;
	}
	onCategoryButtonClick(categoriesState) {
		this.threeWorld.updateVisibleByCategory(categoriesState);
	}
	animate() {
		this.dataGraph.tick();
		this.threeWorld.tick();
		this.windowObject.requestAnimationFrame(this.animate);
	}

	render() {
		if (this.threeWorld) {
			return(
				<ReactContainer
					visible={this.state.activeTweet}
					activeTweet={this.state.activeTweet}
					activeTweetPos={this.state.activeTweetNodePos}
					container={this.domElement3D}
					categories={this.dataGraph.categories}
					tweets={this.dataGraph.nodes}
					onCategoryButtonClick={this.onCategoryButtonClick}
				/>
			);

		} else {
			return <div>{"loading"}</div>;
		}
	}
}
Main.propTypes = {
	windowObject:React.PropTypes.object.isRequired
};
export default Main;
