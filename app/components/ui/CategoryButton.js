import React from "react";
import Props from "prop-types";
const CategoryButton = (props) => {
	const color = "#" + props.color.toString(16);
	return (
		<div
			id={props.id}
			style={{
				padding: "8px 12px",
				borderBottom: "solid #667a8d 1px",
				borderLeft: "solid " + color + " 10px",
				cursor: "pointer",
				backgroundColor:props.active ? "#55728e" : props.hiliteBgColor
			}}
			onClick = {props.onClick}
		>
			<span style={{pointerEvents:"none"}}>{props.name}</span>
		</div>
	);
};
CategoryButton.propTypes = {
	id:Props.string.isRequired,
	name:Props.string.isRequired,
	active:Props.bool,
	hiliteBgColor:Props.string,
	onClick:Props.func.isRequired
};
export default CategoryButton;
