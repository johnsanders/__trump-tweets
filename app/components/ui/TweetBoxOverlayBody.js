import React from "react";
import Props from "prop-types";

class TweetBoxOverlayBody extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="panel-body"
				dangerouslySetInnerHTML={{__html:this.props.text}}
			>
			</div>
		);
	}
}
TweetBoxOverlayBody.propTypes = {
	text:Props.string
};
export default TweetBoxOverlayBody;
