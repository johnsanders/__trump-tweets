import React from "react";
import Props from "prop-types";
import ControlsTop from "./ControlsTop";
import ControlsLeft from "./ControlsLeft";
const Controls = (props) => {
	return (
		<div style={{height:"100%"}}>
			<ControlsTop 
				tweets = {props.tweets}
			/>
			<ControlsLeft
				style={{height:"100%"}}
				categories={props.categories}
				onCategoryButtonClick={props.onCategoryButtonClick}
			/>
		</div>
	);
};
Controls.propTypes = {
	categories:Props.array.isRequired,
	onCategoryButtonClick:Props.func.isRequired
};
export default Controls;
