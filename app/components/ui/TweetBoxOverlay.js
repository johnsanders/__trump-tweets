import React from "react";
import Props from "prop-types";
import TweetBoxOverlayHeading from "./TweetBoxOverlayHeading";
import TweetBoxOverlayBody from "./TweetBoxOverlayBody";
class TweetBoxOverlay extends React.Component {
	constructor(props) {
		super(props);
		this.calcTop = this.calcTop.bind(this);
		this.calcLeft = this.calcLeft.bind(this);
		this.setSize = this.setSize.bind(this);
		this.state = {measurementsReady:false};
	}
	componentDidMount() {
		const bounds = this.props.container.getBoundingClientRect();
		this.offsetX = bounds.left;
		this.offsetY = bounds.top;
		this.containerWidth = this.props.container.offsetWidth;
		this.containerHeight = this.props.container.offsetHeight;
	}
	calcTop() {
		if (this.props.top + this.offsetY < this.containerHeight / 2) {
			return (this.props.top + this.offsetY).toString() + "px";
		} else {
			const selfHeight = this.selfHeight || 200;
			return (this.props.top + this.offsetY - selfHeight).toString() + "px";
		}
	}
	calcLeft() {
		if (this.props.left - this.offsetX < this.containerWidth / 2) {
			return (this.props.left + this.offsetX).toString() + "px";
		} else {
			return (this.props.left + this.offsetX - this.selfWidth).toString() + "px";
		}
	}
	setSize(el) {
		if (el) {
			this.selfWidth = el.offsetWidth;
			this.selfHeight = el.offsetHeight;
			this.setState({measurementsReady:true});
		}
	}
	render() {
		return this.props.tweet ? (
			<div className="panel panel-primary"
				ref={this.setSize}
				style={{
					position:"absolute",
					width:"40%",
					backgroundColor:"rgba(255,255,255,.85)",
					boxShadow: `0px 0px 80px 0px rgba(0,0,0,0.6)`,
					zIndex: "1000",
					top:this.calcTop(),
					left:this.calcLeft(),
				//	opacity:this.state.measurementsReady ? 0 : 1
				}}
			>
				<TweetBoxOverlayHeading created_at={this.props.tweet.created_at} />
				<TweetBoxOverlayBody text={this.props.tweet.text} />
			</div>
		) : null;
	}
}
TweetBoxOverlay.propTypes = {
	tweet:Props.object,
	container:Props.object,
	top:Props.number,
	left:Props.number
};

export default TweetBoxOverlay;
