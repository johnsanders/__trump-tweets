import React from "react";
import Props from "prop-types";
import dateFormat from "dateformat";
class TweetBoxOverlayHeading extends React.Component {
	constructor(props) {
		super(props);
	}
	formatDate(date) {
		return dateFormat(new Date(date), "dddd, mmmm dS, yyyy");
	}
	formatTime(date) {
		return dateFormat(new Date(date), "h:MM:ss TT");
	}

	render() {
		return(
			<div
				className="panel-heading"
				style={{display:"flex", justifyContent:"space-between"}}
			>
				<span>
					{this.formatDate(this.props.created_at)}
				</span>
				<span>
					{this.formatTime(this.props.created_at)}
				</span>
			</div>

		);
	}
}
TweetBoxOverlayHeading.propTypes = {
	created_at:Props.string
};
export default TweetBoxOverlayHeading;
