import React from "react";
import Props from "prop-types";
import {TweenLite} from "gsap";
import numberWithCommas from "number-with-commas";

class StatBox extends React.Component{
	constructor(props){
		super(props);
		this.tweens = {val:0};
		this.state = {val:0};
		this.updateVal = this.updateVal.bind(this);
	}
	componentDidMount(){
		TweenLite.to( this.tweens, 3, {val:this.props.val, onUpdate:this.updateVal} );
	}
	updateVal(val){
		this.setState({val:Math.ceil(this.tweens.val).toString()});
	}
	render(){
		return (
			<div style={{color:"#A0A0A0", boxShadow:"0px 0px 0px 1px #A0A0A0 inset"}}> 
				<div style={{textAlign:"center", paddingLeft:"5px", float:"left", height:"50px", display:"flex", flexDirection:"column", justifyContent:"center"}}>
					<div style={{fontWeight:"bold"}}>{this.props.title.toUpperCase()}</div>
					<div style={{fontSize:"10px", textAlign:"center" }}>{this.props.subtitle.toUpperCase()}</div>
				</div>
				<div style={{width:"125px", textAlign:"center", fontSize:"20px", float:"left", display:"flex", justifyContent:"center", alignItems:"center", height:"50px", marginLeft:"10px", background:"#A0A0A0", color:"#1a2229", padding:"0 5px"}}>
					{numberWithCommas(this.state.val)}
				</div>
			</div>
		);
	}
}
export default StatBox;
