import React from "react";
import TweetBoxOverlay from "./TweetBoxOverlay";
import Controls from "./Controls";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
const ReactContainer = (props) => {
	const overlay = props.activeTweet ?
		<TweetBoxOverlay
			key="1000"
			visible={props.activeTweet}
			tweet={props.activeTweet}
			top={props.activeTweetPos.y}
			left={props.activeTweetPos.x}
			container={props.container}
		/>
	: null;
	return(
		<div style={{height:"100%"}}>
			<CSSTransitionGroup
				transitionName="overlayTrans"
				transitionEnterTimeout={200}
				transitionLeaveTimeout={200}
			>
				{overlay}
			</CSSTransitionGroup>
			<Controls
				categories={props.categories}
				tweets={props.tweets}
				onCategoryButtonClick = {props.onCategoryButtonClick}
			/>
		</div>
	);
}
export default ReactContainer;
