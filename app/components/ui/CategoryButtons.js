import React from "react";
import Props from "prop-types";
import CategoryButton from "./CategoryButton";
class CategoryButtons extends React.Component {
	constructor(props) {
		super(props);
		const categoriesState = {};
		this.categories = [];
		const addCategories = (arr) => {
			arr.forEach(function(category) {
				categoriesState[category.id] = category.initialVisible;
				this.categories.push(category);
				if (category.children.length > 0) {
					addCategories(category.children);
				}
			}.bind(this));
		};
		categoriesState[props.categories[0].id] = true;
		addCategories(props.categories[0].children);
		this.state = Object.assign({}, categoriesState);
		this.render = this.render.bind(this);

		this.onCategoryButtonClick = this.onCategoryButtonClick.bind(this);
		this.onCategoryButtonClick( categoriesState );
	}
	onCategoryButtonClick(e) {
		if ( e.target ){
			const id = e.target.id;
			for (var cat in this.categories) {
				if (this.categories[cat].id == id) {
					this.setState(
						{[id]: !this.state[e.target.id]},
						function() {
							this.props.onCategoryButtonClick(this.state);
						}.bind(this));
					break;
				}
			}
		} else {
			this.props.onCategoryButtonClick(this.state);
		}
	}
	render() {
		return (
			<div style={{borderTop:"solid #667a8d 1px", height:"100%", overflow:"auto"}}>
				{
					this.categories.map((category) => {
						return (
							<CategoryButton
								key={category.id}
								id={category.id}
								name={category.name}
								color={category.color}
								active={ this.state[category.id] }
								onClick={this.onCategoryButtonClick}
							/>
						);
					})
				}
			</div>
		);
	}
}
CategoryButtons.propTypes = {
	categories:Props.array.isRequired,
	onCategoryButtonClick:Props.func.isRequired
};
export default CategoryButtons;
