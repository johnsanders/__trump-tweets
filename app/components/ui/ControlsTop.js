import React from "react";
import Props from "prop-types";
import twitterVerified from "../../images/Verified-Twitter.png";
import trumpImage from "../../images/DJT_Headshot_V2_400x400.jpg";
import calculateTwitterStats from "../../utils/calculateTwitterStats";
import StatBox from "./StatBox";
const ControlsTop = (props) => {
	const twitterStats = calculateTwitterStats(props.tweets);
	return(
		<div 
			height="200"
			style={{minHeight:"50px", color:"#FFF", width:"100%", boxShadow:"0 0 20px #000"}}
		>
			<div style={{display:"flex", flexDirection:"row", width:"200px", float:"left"}}>
				<img height="50" src={trumpImage} />
				<div style={{marginLeft:"5px", weight:"bold"}}>
					<div>
						<div style={{marginTop:"5px"}}>
							Donald J. Trump&nbsp;	
							<img src={twitterVerified} style={{height:"16px"}}/>
						</div>
						<div style={{fontSize:"12px", color:"#808080"}}>
							@realDonaldTrump
						</div>
					</div>
				</div>
			</div>
			<div style={{display:"flex", flexDirection:"row", justifyContent:"space-between", padding:"0 20px"}}>
				<div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
					<StatBox title="TWEETS" subtitle="AS PRESIDENT" val={twitterStats.tweets} />	
				</div>
				<div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
					<StatBox title="FAVORITES" subtitle="AS PRESIDENT" val={twitterStats.favorites} />	
				</div>
				<div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
					<StatBox title="RETWEETS" subtitle="AS PRESIDENT" val={twitterStats.retweets} />	
				</div>
			</div>

		</div>
	);
}
ControlsTop.propTypes = {

}
export default ControlsTop;
