import React from "react";
import Props from "prop-types";
import CategoryButtons from "./CategoryButtons";
const ControlsLeft = (props) => {
	return(
		<div style={{
			background: "#2D353C",
			color: "#FFF", 
			width: 200,
			marginTop:"20px", 
			boxShadow:"0 0 20px #000",
			height:"100%"
		}}>
			<CategoryButtons
				categories={props.categories}
				highlightBgColor="#00bcd4"
				onCategoryButtonClick={props.onCategoryButtonClick}
			/>
		</div>
	);
};
ControlsLeft.propTypes = {
	categories:Props.array.isRequired,
	onCategoryButtonClick:Props.func.isRequired
};
export default ControlsLeft;
