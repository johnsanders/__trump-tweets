import {TextGeometry, Mesh} from "three";
import {labelTextMaterial, cnnSansCondensed} from "../../utils/sharedAssets";
class CategoryLabelText extends Mesh {
	constructor(text) {
		const textGeo = new TextGeometry(text.toUpperCase(), {
			font: cnnSansCondensed,
			size: 60,
			height: 5,
			curveSegments: 4,
			bevelThickness: 1,
			bevelSize: 0.8
		});
		super(textGeo, labelTextMaterial);

		textGeo.computeBoundingBox();
		this.width = textGeo.boundingBox.max.x - textGeo.boundingBox.min.x;
		this.height = textGeo.boundingBox.max.y - textGeo.boundingBox.min.y;
		this.position.x -= this.width/2;
		this.position.y -= this.height/2;
	}
}
export default CategoryLabelText;
