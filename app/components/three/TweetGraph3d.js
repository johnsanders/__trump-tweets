import NodeMeshFactory from "../../factories/NodeMeshFactory.js";
import NodeMeshes from "./NodeMeshes";
import EdgeLines from "./EdgeLines";
import Selector3D from "../../utils/Selector3D";
class TweetGraph3d {
	constructor(dataGraph, tweetClickCallback, tweetHoverCallback, camera, domElement) {
		// BIND METHODS
		this.onNodeClick = this.onNodeClick.bind(this);
		this.onNodeHover = this.onNodeHover.bind(this);
		this.tick = this.tick.bind(this);
		this.tweetClickCallback = tweetClickCallback;
		this.tweetHoverCallback = tweetHoverCallback;

		// CREATE 3D NODES
		this.tweetNodesContainer = new NodeMeshes(dataGraph.nodes);
		
		// CREATE 3D EDGE LINES
		this.edgesContainer = new EdgeLines(dataGraph.edges, camera.position);

		// BIND MOUSE/TOUCH EVENTS IN 3D
		this.selector3D = new Selector3D({
			container: this.tweetNodesContainer,
			camera: camera, domElement: domElement,
			callbackMouseMove: this.onNodeHover,
			callbackClick: this.onNodeClick
		});
		this.activeTweet = null;
	}
	attachListeners() {
		this.selector3D.init();
	}
	onNodeHover(obj) {
		if (obj !== this.activeTweet) {
			this.activeTweet = obj;
			if (obj === null) {
				this.tweetHoverCallback(null);
			} else {
				this.tweetHoverCallback(obj.userData.dataNode, obj);
			}
		}
	}
	onNodeClick(obj) {
		this.tweetClickCallback(obj.userData.dataNode, obj);
	}
	updateVisibleByCategory(categories) {
		this.tweetNodesContainer.updateVisibleByCategory(categories);
		this.edgesContainer.updateVisibleByCategory(categories);
	}
	tick() {
		this.edgesContainer.tick();
		this.tweetNodesContainer.tick();
	}
}
export default TweetGraph3d;




