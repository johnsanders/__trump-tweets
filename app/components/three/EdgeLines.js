import {Object3D} from "three";
import {edgeLineGeometry, edgeLineMaterial} from "../../utils/sharedAssets.js";
import EdgeLine from "./EdgeLine";

class EdgeLines extends Object3D {
	constructor(edges, cameraPosition) {
		super();
		edges.forEach( (edge) => {
			this.addEdgeLine(edge.source, edge.target);
		} );
	}
	addEdgeLine(source, target) {
		const edgeLine = new EdgeLine(edgeLineGeometry, edgeLineMaterial);
		edgeLine.userData.source = source;
		edgeLine.userData.target = target;
		this.setEdgePandP(edgeLine);
		this.add(edgeLine);
	}
	updateVisibleByCategory(categories) {
		this.children.forEach((line) => {
			line.setVisible(categories, Math.random() );
		});
	}
	setEdgePandP(edgeLine) {
		if (!edgeLine.transitioning) {
			edgeLine.scale.set(1, 1, this.getDistance(edgeLine.userData.source.position,
			edgeLine.userData.target.position));
		}
		edgeLine.position.x = edgeLine.userData.target.position.x;
		edgeLine.position.y = edgeLine.userData.target.position.y;
		edgeLine.position.z = edgeLine.userData.target.position.z;
		edgeLine.lookAt(edgeLine.userData.source.data.node3d.position);
	}
	getDistance(a,b) {
		return a.distanceTo(b);
	}
	getMiddle (a,b) {
		return (a+b)/2;
	}
	tick() {
		this.children.forEach((edge) => {
			this.setEdgePandP(edge);
		});
	}
}
export default EdgeLines;
