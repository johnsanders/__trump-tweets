import {LineSegments} from "three";
import {TweenLite} from "gsap";
class EdgeLine extends LineSegments {
	constructor(geometry, material) {
		super(geometry, material);
		this.transitioning = false;
		this.afterAnimate = this.afterAnimate.bind(this);
		this.scale.z = 0.0001;
		this.visible = false;
	}
	setVisible(state, delay) {
		let sourceActive, targetActive;
		if ( state.constructor === Object ) {
			sourceActive = this.userData.source.isActive(state);
			targetActive = this.userData.target.isActive(state);
		} else if ( typeof state === "boolean" ){
			sourceActive = targetActive = state;
		}
		const lineActive = sourceActive && targetActive;
		if ( lineActive != this.visible ){
			let targetScale;  
			this.transitioning = true;
			if (lineActive){
				targetScale = 500;
				this.visible = true;
				this.scale.z = 0.0001;
			} else {
				targetScale = 0.00001;
			}
			TweenLite.to( this.scale, 0.3, {z:targetScale, delay:delay, 
				onComplete:this.afterAnimate, onCompleteParams:[lineActive]} );
		}
	}
	afterAnimate(isVisible) {
		this.transitioning = false;
		this.visible = isVisible;
	}
}
export default EdgeLine;
