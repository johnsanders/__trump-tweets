import {CubeTextureLoader} from "three";
import xPos from "../../images/xPos.jpg";
import xNeg from "../../images/xNeg.jpg";
import yPos from "../../images/yPos.jpg";
import yNeg from "../../images/yNeg.jpg";
import zPos from "../../images/zPos.jpg";
import zNeg from "../../images/zNeg.jpg";

export default new CubeTextureLoader().load([xPos, xNeg, yPos, yNeg, zPos, zNeg]);

