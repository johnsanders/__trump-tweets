import {Mesh} from "three";
import {TweenLite} from "gsap";
import CategoryLabel from "./CategoryLabel";
class TweetNodeMesh extends Mesh {
	constructor(node, geometry, material) {
		super(geometry, material);
		this.name = node.id;
		this.userData.dataNode = node;
		this.scale.set(0.00001, 0.00001, 0.00001);
		this.visible = false;
		this.posHistory = [{x:0, y:0, z:0}, {x:0, y:0, z:0}];
		this.onBeforeAnimate = this.onBeforeAnimate.bind(this);
		this.onAfterAnimate = this.onAfterAnimate.bind(this);
	}
	setVisible(state, delay) {
		let isActive;
		if ( state.constructor === Object){
			isActive = this.userData.dataNode.isActive(state);
		} else if ( typeof state === "boolean" ){
			isActive = state;
		}
		this.userData.dataNode.setActive(isActive);
		if ( isActive != this.visible ){
			let targetScale;
			if (isActive){
				targetScale = 1;
				this.visible = true;
			} else {
				targetScale = 0.0001;
			}
			TweenLite.to( this.scale, .3, {x:targetScale, y:targetScale, z:targetScale, 
				delay:delay, ease:Back.easeInOut, 
				onStart:this.onBeforeAnimate, onStartParams:[isActive],
				onComplete:this.onAfterAnimate, onCompleteParams:[isActive]} );
		}
	}
	onBeforeAnimate(isActive){
		if (isActive){
			this.visible = true;
			this.userData.dataNode.setActive(true);	
		}
	}
	onAfterAnimate(isActive) {
		if (!isActive){
			this.visible = false;
			this.userData.dataNode.setActive(true);
		}
	}
	tick(){
		this.rotation.y += 0.01;
		if (this.rotation.y > Math.PI*2){
			this.rotation.y = 0;
		}
		this.setAveragePos();
		this.posHistory.push();
	}
	setAveragePos(){
		this.newX = (this.position.x + this.posHistory[0].x + this.posHistory[1].x) / 3;
		this.newY = (this.position.y + this.posHistory[0].y + this.posHistory[1].y) / 3;
		this.newZ = (this.position.z + this.posHistory[0].z + this.posHistory[1].z) / 3;
		this.posHistory[1].x = this.posHistory[0].x;
		this.posHistory[1].y = this.posHistory[0].y;
		this.posHistory[1].z = this.posHistory[0].z;
		this.posHistory[0].x = this.position.x;
		this.posHistory[0].y = this.position.y;
		this.posHistory[0].z = this.position.z;
		this.position.set(this.newX, this.newY, this.newZ);
	}
}
export default TweetNodeMesh;

