import {PointLight, AmbientLight} from "three";
import ThreeCore from "../../utils/ThreeCore";
import PostProcessing from "./PostProcessing.js";
import TweetGraph3d from "./TweetGraph3d";
import OrbitControls from "three-orbitcontrols";
import ovalOfficeSkybox from "./ovalOfficeSkybox";

class ThreeWorld {
	constructor(tweetDataGraph, domElement, width, height, tweetClickCallback, tweetHoverCallback) {
		this.threeCore = new ThreeCore(width, height);
		this.domElement = this.threeCore.renderer.domElement;
		this.controls = new OrbitControls(this.threeCore.camera, this.threeCore.renderer.domElement);
		this.controls.enableDamping = true;
		this.controls.dampingFactor = 0.25;
		this.controls.minDistance = 500;
		this.controls.maxDistance = 5000;
		this.controls.zoomSpeed = 0.5;
		this.controls.minPolarAngle = Math.PI * 0.3;
		this.controls.maxPolarAngle = Math.PI * 0.7;
		this.controls.minAzimuthAngle = -Math.PI / 2 * 0.6;
		this.controls.maxAzimuthAngle = Math.PI / 2 * 0.6;
		this.threeCore.camera.position.set(0,0,3000);
		const pointLight = new PointLight();
		const ambLight = new AmbientLight(0x606060);
		pointLight.position.set(300, 300, 5000);
		this.threeCore.scene.add(pointLight, ambLight);
		this.threeCore.scene.background = ovalOfficeSkybox;

		this.tweetGraph3D = new TweetGraph3d(tweetDataGraph, tweetClickCallback, tweetHoverCallback, 
			this.threeCore.camera, this.threeCore.renderer.domElement);
		this.threeCore.scene.add(this.tweetGraph3D.tweetNodesContainer);
		this.threeCore.scene.add(this.tweetGraph3D.edgesContainer);

		//this.postProcessing = new PostProcessing(this.threeCore.renderer, this.threeCore.scene,
	//		this.threeCore.overlayScene, this.threeCore.camera);
	}
	componentDidMount(){
		this.tweetGraph3D.revealAll();
	}
	onDomReady() {
		this.tweetGraph3D.attachListeners();
	}
	updateVisibleByCategory(categories) {
		this.tweetGraph3D.updateVisibleByCategory(categories);
	}
	tick() {
		this.controls.update();
		this.tweetGraph3D.tick();
		this.threeCore.render();
		//this.postProcessing.render();
	}
}
export default ThreeWorld;
