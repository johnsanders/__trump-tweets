import {Object3D, Mesh, MeshLambertMaterial} from "three";
import CategoryLabelText from "./CategoryLabelText";
import {labelBackerGeometry, labelBackerMaterial, labelCubeGeometry} from "../../utils/sharedAssets.js";
import {TweenLite} from "gsap";

class CategoryLabel extends Object3D {
	constructor(node, material) {
		super();
		this.name = node.id;
		this.userData.dataNode = node;
		const text = new CategoryLabelText(node.data.info.name);
		text.position.x -= text.width/2 + 20;
		text.position.y += text.height/2 + 20;

		const backer = new Mesh(labelBackerGeometry, labelBackerMaterial);
		backer.position.x -= text.width/2 + 20;
		backer.position.y += text.height/2 + 20;
		backer.position.z = -10;
		backer.scale.set(text.width + 20, text.height + 20, 1);

		const cube = new Mesh(labelCubeGeometry, new MeshLambertMaterial({color:node.data.info.color}));

		this.add(text);
		this.add(backer);
		this.add(cube);
		this.position.x -=  text.width/2;
		this.position.y += text.height/2;
		this.onBeforeAnimate = this.onBeforeAnimate.bind(this);
		this.onAfterAnimate = this.onAfterAnimate.bind(this);
		this.scale.set(0.001, 0.001, 0.001);
		this.visible = false;
	}
	setVisible(state, delay) {
		let isActive;
		if ( state.constructor === Object) {
			isActive = this.userData.dataNode.isActive(state);
		} else if ( typeof state === "boolean" ){
			isActive = state;
		}
		if ( isActive != this.visible ) {
			let targetScale;
			if (isActive){
				targetScale = 1;
				this.visible = true;
			} else {
				targetScale = 0.0001;
			}
			TweenLite.to( this.scale, .3, {x:targetScale, y:targetScale, z:targetScale, 
				ease:Back.easeInOut, delay:delay,
				onStart:this.onBeforeAnimate, onStartParams:[isActive],
				onComplete:this.onfterAnimate, onCompleteParams:[isActive]
			});
		}
	}
	onBeforeAnimate(isActive){
		if(isActive){
			this.visible = true;
			this.userData.dataNode.setActive(true);
		}
	}
	onAfterAnimate(isActive) {
		if(!isActive){
			this.visible = false;
			this.userData.dataNode.setActive(false);
		}
	}
	tick(){

	}

}
export default CategoryLabel;
