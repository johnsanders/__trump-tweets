import {Object3D} from "three";
import NodeMeshFactory from "../../factories/NodeMeshFactory.js"
class NodeMeshes extends Object3D {
	constructor(dataNodes) {
		super();
		const nodeMeshFactory = new NodeMeshFactory();
		const allNodes = nodeMeshFactory.createNodes(dataNodes);
		this.add(...allNodes.map((node) => {return node.data.node3d;}));

	}
	updateVisibleByCategory(categories) {
		this.children.forEach( (node) => {
			node.setVisible(categories, Math.random());
		});
	}
	tick(){
		this.children.forEach((node) => {
			node.tick();
		});
	}
}
export default NodeMeshes;
