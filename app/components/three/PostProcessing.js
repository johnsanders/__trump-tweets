import EffectComposer from "../../vendors/postprocessing/EffectComposer";
import RenderPass from "../../vendors/postprocessing/RenderPass";
import ShaderPass from "../../vendors/postprocessing/ShaderPass";
import HorizontalBlurShader from "../../vendors/shaders/HorizontalBlurShader";
import VerticalBlurShader from "../../vendors/shaders/VerticalBlurShader";
import AdditiveBlendShader from "../../vendors/shaders/AdditiveBlendShader.js";
import {WebGLRenderTarget, LinearFilter, RGBFormat} from "three";
class PostProcessing {
	constructor(renderer, scene, overlayScene, camera){
		this.renderer = renderer;
		this.scene = scene;
		this.camera = camera;
		const renderTargetParameters = 
			{ minFilter: LinearFilter, magFilter: LinearFilter, 
			format: RGBFormat, stencilBuffer: false };

		this.mainRenderTarget = new WebGLRenderTarget( 1920, 1080, renderTargetParameters );

		const overlayRenderTarget = new WebGLRenderTarget( 1920, 1080, renderTargetParameters  );
		this.overlayComposer = new EffectComposer(renderer, overlayRenderTarget);
		const overlayRenderPass = new RenderPass(overlayScene, camera);
		this.overlayComposer.addPass(overlayRenderPass);
	
		const hBlur = new ShaderPass( HorizontalBlurShader );
		const vBlur = new ShaderPass( VerticalBlurShader );
		hBlur.uniforms[ "h"  ].value = 2 /1920; 
		vBlur.uniforms[ "v"  ].value = 2 /1080;
		this.overlayComposer.addPass( hBlur );
		this.overlayComposer.addPass( vBlur )
		
		this.finalComposer = new EffectComposer(renderer);
		const blendPass = new ShaderPass( AdditiveBlendShader);
		blendPass.uniforms[ 'tBase' ].value = this.mainRenderTarget.texture;
		blendPass.uniforms[ 'tAdd'  ].value = this.overlayComposer.renderTarget1.texture;
		blendPass.renderToScreen = true;
		this.finalComposer.addPass( blendPass  );
	}
	render(){
		this.renderer.render( this.scene, this.camera, this.mainRenderTarget);
		this.overlayComposer.render()
		this.finalComposer.render();
	}
}

export default PostProcessing;
