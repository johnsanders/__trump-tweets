import {Vector2, Raycaster} from "three";
class Selector3D {
	constructor(parameters) {
		this.domElement = parameters.domElement;
		const raycaster = new Raycaster();
		const mouse = new Vector2();
		let intersected = null;
		let intersects = [];

		const onDocumentMouseMove = (event) => {
			mouse.x = ((event.clientX - this.domOffsetX)/ this.domElementWidth) * 2 - 1;
			mouse.y = - ((event.clientY - this.domOffsetY) / this.domElementHeight) * 2 + 1;
			raycaster.setFromCamera(mouse, parameters.camera);
			intersects = raycaster.intersectObjects(parameters.container.children);

			if(intersects.length > 0) {
				if (intersected != intersects[0].object) {
					intersected = intersects[ 0 ].object;
				}
			} else {
				intersected = null;
			}
			parameters.callbackMouseMove(intersected);
		};
		parameters.domElement.addEventListener("mousemove", onDocumentMouseMove, false);

		const onDocumentMouseClick = () => {
			if(intersected) {
				parameters.callbackClick(intersected);
			}
		};
		parameters.domElement.addEventListener("click", onDocumentMouseClick, false);
	}
	init() {
		this.domOffsetX = this.domElement.getBoundingClientRect().left;
		this.domOffsetY = this.domElement.getBoundingClientRect().top;
		this.domElementWidth = this.domElement.offsetWidth;
		this.domElementHeight = this.domElement.offsetHeight;
		this.ready = true;
	}
}
export default Selector3D;
