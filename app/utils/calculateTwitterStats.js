export default (tweets) => {
	const ret = {tweets:0, favorites:0, retweets:0};
	tweets.forEach(function(tweet) {
		if (tweet.data.type == "tweet") {
			ret.tweets++;
			ret.favorites += tweet.data.info.data.favorite_count;
			ret.retweets += tweet.data.info.data.retweet_count;
		}
	});
	return ret;
};
