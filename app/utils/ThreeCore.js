import {PerspectiveCamera, Scene, WebGLRenderer} from "three";
class ThreeCore {
	constructor(width, height) {
		this.renderer = new WebGLRenderer({antialias:true});
		this.renderer.clearColor = 0x000000;
		this.renderer.setSize(width, height);
		this.camera = new PerspectiveCamera(60, 16/9, 1, 1000000);
		this.scene = new Scene();
		this.overlayScene = new Scene();
		this.render = this.render.bind(this);
	}
	render() {
		this.renderer.render(this.scene, this.camera);
	}
}
export default ThreeCore;
