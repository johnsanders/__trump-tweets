export default (color1,color2) => {
	const avg = (a,b) => {
		return (a+b)/2;
	};
	const t16 = (c) => {
		return parseInt((""+c).replace("#",""),16);
	};
	const hex = (c) => {
		const t = (c>>0).toString(16);
		return t.length == 2 ? t : "0" + t;
	};
	const hex1 = t16(color1);
	const hex2 = t16(color2);
	const r = (hex) => {
		return hex >> 16 & 0xFF;
	};
	const g = (hex) => {
		return hex >> 8 & 0xFF;
	};
	const b = (hex) => {
		return hex & 0xFF;
	};
	const res = "#" + hex(avg(r(hex1),r(hex2))) + hex(avg(g(hex1),g(hex2))) + hex(avg(b(hex1),b(hex2)));
	return res;
};
