import {MeshLambertMaterial, LineBasicMaterial, Geometry, Vector3, PlaneGeometry, BoxGeometry, Font} from "three";
import cnnSansCondensedJson from "../fonts/CNN Sans Condensed Medium_Regular.json";

export const cnnSansCondensed = new Font(cnnSansCondensedJson);
export const labelTextMaterial = new MeshLambertMaterial({color:0xFFFFFF});
export const labelBackerMaterial = new MeshLambertMaterial({color:0x000000});
export const labelBackerGeometry = new PlaneGeometry(1,1);
export const labelCubeGeometry = new BoxGeometry(30,30,30);
export const edgeLineMaterial = new LineBasicMaterial({color:0xFFFFFF});
const edgeLineGeometry = new Geometry();
edgeLineGeometry.vertices.push(new Vector3(0,0,0));
edgeLineGeometry.vertices.push(new Vector3(0,0,1));
export {edgeLineGeometry};


