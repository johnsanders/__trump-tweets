import {SphereGeometry, MeshLambertMaterial, Color, FlatShading} from "three";
import TweetNodeMesh from "../components/three/TweetNodeMesh";
import CategoryLabel from "../components/three/CategoryLabel";
import averageColor from "../utils/averageColor";
class NodeMeshFactory {
	constructor() {
		this.sphereGeometry = new SphereGeometry(50);
		this.sphereGeometry.computeFlatVertexNormals();
		this.materials = {
			root: new MeshLambertMaterial({color:0xFF0000})
		};
	}
	createNodes(nodes) {
		nodes.forEach(function(node, index, arr) {
			switch(node.data.type) {
			case "trump":
				arr[index].data.node3d = this.createRootNode(node, 2000);
				break;
			case "category":
				arr[index].data.node3d = this.createCategoryNode(node, 2000);
				break;
			case "tweet":
				arr[index].data.node3d = this.createTweetNode(node, 2000);
				break;
			}
			arr[index].position = arr[index].data.node3d.position;
		}.bind(this));
		return nodes;
	}
	createRootNode(node, volume) {
		const nodeMesh = new TweetNodeMesh(node, this.sphereGeometry, this.materials.root);
		const rand = this.getRandomPos3(volume);
		nodeMesh.position.set(...rand);
		return nodeMesh;

	}
	createCategoryNode(node, volume) {
		if (!this.materials.hasOwnProperty(node.data.info.name)) {
			this.materials[node.data.info.name] = new MeshLambertMaterial({color:node.data.info.color, shading:FlatShading});
		}
		const categoryMesh = new CategoryLabel(node, this.materials[node.data.info.name]);
		const rand = this.getRandomPos3(volume);
		categoryMesh.position.set(...rand);
		return categoryMesh;
	}
	createTweetNode(node, volume) {
		let colorName = node.categories[0].name;
		if (node.categories.length == 1) {
			if (!this.materials.hasOwnProperty(node.categories[0].name)) {
				this.materials[node.categories[0].name] = new MeshLambertMaterial({color:node.categories[0].color, shading:FlatShading});
			}
		} else {
			let colorAvgString = node.categories[0].color.toString(16);
			node.categories.forEach(function(category, index) {
				if (index > 0) {
					colorAvgString = averageColor(colorAvg, category.color);
					colorName += "_" + category.name;
				}
			});
			const colorAvg = new Color(colorAvgString);
			if (!this.materials.hasOwnProperty(colorName)) {
				this.materials[colorName] = new MeshLambertMaterial({color:colorAvg, shading:FlatShading});
			}
		}
		const nodeMesh = new TweetNodeMesh(node, this.sphereGeometry, this.materials[colorName]);
		nodeMesh.shading = FlatShading;
		const rand = this.getRandomPos3(volume);
		nodeMesh.position.set(...rand);

		if (node.data.info.favScale) {
			const scale = 1 + node.data.info.favScale * 4;
			nodeMesh.scale.set(scale, scale, scale);
		}

		return nodeMesh;
	}
	getRandomPos(vol) {
		return Math.floor((Math.random()*vol*2)-vol);
	}
	getRandomPos3(vol) {
		return [this.getRandomPos(vol), this.getRandomPos(vol), this.getRandomPos(vol)];
	}

}
export default NodeMeshFactory;
