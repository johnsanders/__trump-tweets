import TweetNode from "../components/dataGraph/TweetNode";
class DataNodeAndEdgeFactory {
	static createTweets(tweets, categories, categoryNodes, layout) {
		const getMatchingTweets = (tweets, categoryInfo) => {
			return tweets.filter((tweet) => {
				for (let keyword of categoryInfo.keywords) {
					if (new RegExp(`\\b${keyword}\\b`, "i").test(tweet.text)) {
						tweet.hasCategoryMatch = true;
						return true;
					}
				}
				return false;
			});
		};

		let otherCategory;
		let count = categoryNodes.length;
		const ret = {nodes:[], edges:[]};
		categoryNodes.forEach((node) => {
			if (node.data.type == "category") {
				if (node.data.info.name == "Other") {
					otherCategory = node;
					return;
				}
				const matchingTweets = getMatchingTweets(tweets, node.data.info);
				matchingTweets.forEach((tweet) => {
					let tweetNode;
					if (tweet.graphNode) {
						tweetNode = tweet.graphNode;
					} else {
						tweetNode = new TweetNode(count, tweet, layout);
						tweet.graphNode = tweetNode;
					}
					tweetNode.addCategory(node.data.info);
					ret.nodes.push(tweetNode);
					ret.edges.push([node, tweetNode]);
					count++;
				});
			}
		});
		const unmatchedTweets = tweets.filter((tweet) => {
			return !tweet.hasCategoryMatch;
		});

		unmatchedTweets.forEach((tweet) => {
			const tweetNode = new TweetNode(count, tweet, layout);
			tweetNode.addCategory(otherCategory.data.info);
			ret.nodes.push(tweetNode);
			ret.edges.push([otherCategory, tweetNode]);
			count++;
		});
		return ret;
	}

}
export default DataNodeAndEdgeFactory;
