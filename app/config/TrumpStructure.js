import shortid from "shortid";
class TrumpStructure {
	constructor() {
		this.addUniqueIds = this.addUniqueIds.bind(this);
		this.data = [
			{
				name:"Trump",
				color:0xFF0000,
				type:"people",
				keywords:[],
				initialVisible:true,
				children: [
					{
						name:"Immigration",
						color:0x91db02,
						type:"topic",
						initialVisible:true,
						keywords:["immigration", "immigrants", "sanctuary"],
						children:[
							{
								name:"Border Wall",
								color:0x25ad00,
								type:"topic",
								keywords:["wall", "border", "borders"],
								initialVisible:true,
								children:[]
							},
							{
								name:"Refugees",
								color:0x25ad00,
								type:"topic",
								keywords:["refugee", "refugees", "vetting"],
								initialVisible:true,
								children:[]
							}
						]
					},
					{
						name:"News Media",
						color:0x01a7e5,
						type:"institution",
						keywords:[ "tv", "network", "networks", "cnn", "fox", "foxnews", "fox&friends", "foxandfriends", "nbc", "msnbc", "cbs",
							"nytimes", "ny times", "newyorktimes", "washington post"],
						initialVisible:true,
						children:[{
							name:"Fake News",
							color:0x042198,
							type:"topic",
							initialVisible:true,
							keywords:["fakenews", "fake news"],
							children:[]
						}]
					},
					{
						name:"Terrorism",
						color:0x303030,
						type:"topic",
						keywords:["terrorism", "terrorist", "terrorists", "isis", "qaeda", "bin laden"],
						initialVisible:true,
						children:[]
					},
					{
						name:"Jobs",
						color:0x78FF32,
						type:"topic",
						keywords:[ "JOBS!", "jobs", "employment", "unemployment" ],
						initialVisible:true,
						children:[]
					},
					{
						name:"Russia",
						color:0xFF0324,
						type:"topic",
						keywords:["Russia", "Putin"],
						initialVisible:true,
						children:[]
					},
					{
						name:"Wiretapping",
						color:0x89a0FF,
						type:"topic",
						keywords:["wire tapping", "unmask", "unmasking", "unmasked", "wiretap", "wiretapping", "wiretapped", "surveillance", "surveil", "spy"],
						initialVisible:true,
						children:[]
					},
					{name:"Syria",
						color:0xA08757,
						type:"topic",
						keywords:["syria", "syrian", "syrians", "assad"],
						initialVisible:true,
						children:[]
					},
					{
						name:"Democrats",
						color:0x3111FF,
						type:"institution",
						keywords:["dems", "democrats", "democrat", "democratic", "schumer", "pelosi"],
						initialVisible:true,
						children:[
							{
								name:"Obama",
								color:0x38a8f1,
								type:"people",
								keywords:["obama", "obamacare"],
								initialVisible:true,
								children:[]
							},
							{
								name:"Clintons",
								color:0xe59201,
								type:"people",
								keywords:["clinton", "hillary"],
								initialVisible:true,
								children:[]
							}

						]
					},
					{
						name:"MAGA",
						color:0x5819a9,
						type:"topic",
						keywords:["MAGA", "#MAGA", "make america great again", "making america great again"],
						initialVisible:true,
						children:[]
					},
					{
						name:"Other",
						color:0x909090,
						type:"topic",
						keywords:[],
						initialVisible:false,
						children:[]
					}
				]
			}
		];
		this.data = this.addUniqueIds(this.data);
	}
	addUniqueIds (arr) {
		arr.forEach((item, index, arr) => {
			arr[index].id = "category_" + shortid.generate();
			if (item.children.length) {
				this.addUniqueIds(item.children);
			}
		});
		return arr;
	}
}
export default TrumpStructure;
